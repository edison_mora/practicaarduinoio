#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>

#include "arduino-serial-lib.h"

#define MAX_TEMP 13
#define MAX_HUM 13

float calculateSD(int data[]);
int valorMinimo(int a[], int n);
int valorMaximo(int a[], int n);
void imprimirArray(int a[], int n);
float mediaArray(int a[], int n);

void error(char* msg){
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]){
    int fd = -1;
    int baudrate = 9600;  // default
    
    int temperaturas[MAX_TEMP];
    int temperatura = 0;   //Variable que va a almacenar la temperatura del arduino
    
    int humedades[MAX_HUM];
    int humedad = 0;       //Variable que va a almacenar la humedad del arduino
    
    char tempc = 't';       //Comando para leer temperatura del arduino
    char humc = 'h';        //Comando para leer humedad del arduino

    /* VARIABLES PARA ESTADÍSTICAS */
    int maxTemperatura = 0;
    int minTemperatura = 0;
    int maxHumedad = 0;
    int minHumedad = 0;
    float mediaTemperatura = 0.00;
    float mediaHumedad = 0.00;
    float sdTemperatura = 0.00;
    float sdHumedad = 0.00;
    /*------------------------------*/
    int contador = 0;
    int salida = 0;

    fd = serialport_init("/dev/ttyACM0", baudrate);

    if( fd==-1 ){
        error("couldn't open port");
        return -1;
    }else{
        serialport_flush(fd);
        //Se leen los valores del arduino por 1 minuto
        while (contador < 13){
            //Coge la temperatura del arduino
            write(fd, &tempc, 1);
            read(fd, &temperatura , 1);
            temperaturas[contador] = temperatura;   //Añado el valor de la temperatura obtenido al array
            printf("Temperatura: %d\n", temperatura );
            //Coge la humedad del arduino
            write(fd, &humc, 1);
            read(fd, &humedad, 1);
            humedades[contador] = humedad;  //Añado el valor de la humedad obtenido al array
            printf("Humedad: %d\n", humedad );
            //Espera 5 segundos apra volver a comenzar
            sleep(5);
            contador++;
        }
        /*
            ESTADÍSTICAS DE TEMPERATURA
        */
        printf("Los valores de temperatura son:\n");
        imprimirArray(temperaturas, MAX_TEMP);
        //Temperatura maxima
        maxTemperatura = valorMaximo(temperaturas, MAX_TEMP);
        printf("El valor máximo de temperaturas es: %d\n", maxTemperatura);
        //Temperatura mínima
        minTemperatura = valorMinimo(temperaturas, MAX_TEMP);
        printf("El valor mínimo de temperaturas es: %d\n", minTemperatura);
        //Media
        mediaTemperatura = mediaArray(temperaturas, MAX_TEMP);
        printf("El valor de la media de las temperaturas es: %f\n", mediaTemperatura);
        //Desviación estándar
        sdTemperatura = calculateSD(temperaturas);
        printf("El valor de la desviación estándar de la temperatura es: %f\n", sdTemperatura);
        /*
            ESTADÍSTICAS DE HUMEDAD
        */
        printf("Los valores de humedad son:\n");
        imprimirArray(humedades, MAX_HUM);
        //Humedad maxima
        maxHumedad = valorMaximo(humedades, MAX_HUM);
        printf("El valor máximo de humedades es: %d\n", maxHumedad);
        //Humedad mínima
        minHumedad = valorMinimo(humedades, MAX_HUM);
        printf("El valor mínimo de humedades es: %d\n", minHumedad);
        //Media
        mediaHumedad = mediaArray(humedades, MAX_HUM);
        printf("El valor de la media de las humedades es: %f\n", mediaHumedad);
        //Desviación estándar
        sdHumedad = calculateSD(humedades);
        printf("El valor de la desviación estándar de la humedad es: %f\n", sdHumedad);

    }
    close( fd );
    printf("Presione 0 para salir\n");
    scanf("%d", &salida);
    return 0;  
}

/* Ejemplo para calcular desviacion estandar y media */
float calculateSD(int data[]){
    float sum = 0.0, mean, standardDeviation = 0.0;

    int i;

    for(i = 0; i < 10; ++i)
    {
        sum += data[i];
    }

    mean = sum/10;

    for(i = 0; i < 10; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation / 10);
}
/* Función para calcular el valor mínimo de cualquier array */
int valorMinimo(int a[], int n){
    int min;
    min = a[0];
    for (int i = 0; i < n; i++){
        if(a[i] < min){
            min = a[i];
        }
    }
    return min;
}
/* Función para calcular el valor máximp de cualquier array */
int valorMaximo(int a[], int n){
    int max;
    max = a[0];
    for (int i = 0; i < n; i++){
        if(a[i] > max){
            max = a[i];
        }
    }
    return max;
}
/*Función para encontrar la media de los valores de cualquier array*/
float mediaArray(int a[], int n){
    float media = 0.00;
    int sum = 0;
    for (int i = 0; i < n; i++){
        sum += a[i];
    }
    media = (float)sum/n;
    return media;
}
/* Función para imprimir los valores de cualquier array */
void imprimirArray(int a[], int n){
    printf("[ ");
    for (int i = 0; i < n; i++){
        printf(" %d,", a[i]);
    }
    printf(" ]\n");
}